<?php

/*
 * Blacklist Model
 */

class Blacklist_model extends CI_Model {

    private $tableName = 'u_m_blacklist';

    public function __construct() {
        parent::__construct();
    }

    public function getList($opn_id) {
        $where['opn_id'] = $opn_id;

        $result = $this->db->get_where($this->tableName, $where, 1);
        if ($result->num_rows() > 0) {
            return $result->row();
        }
        return FALSE;
    }

}