<?php

class Cmd_response_model extends CI_Model {

    private $tableName = 'u_m_cmd_response';

    public function __construct() {
        parent::__construct();
    }

    public function getResponse($for_action_key, $is_byid = 0) {
        if (1 == $is_byid) {
            $where['id'] = $for_action_key;
        } else {
            $where['for_action_key'] = $for_action_key;
        }

        $result = $this->db->get_where($this->tableName, $where);

        if ($result->num_rows() > 0) {
            return $result->row();
        }

        return FALSE;
    }

}

