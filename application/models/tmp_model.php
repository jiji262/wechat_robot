<?php

class Tmp_model extends CI_Model {

    private $tableName = 'u_m_tmp';

    public function __construct() {
        parent::__construct();
    }

    public function getTmp($user_id) {
        $where['user_id'] = $user_id;

        $result = $this->db->get_where($this->tableName, $where, 1);

        if ($result->num_rows() > 0) {
            return $result->row();
        }
        return FALSE;
    }

    public function setTmp($user_id, $action_key = '', $need_confirm = 0, $json_data = '') {
        $where['user_id'] = $user_id;

        $set['update_time'] = time();
        if ($action_key !== '') {
            $set['action_key'] = $action_key;
        }

        if ($json_data !== '') {
            $set['json_data'] = $json_data;
        }

        $set['need_confirm'] = $need_confirm;

        if (TRUE === $this->db->update($this->tableName, $set, $where, 1)) {
//            return $this->db->last_query();
            return TRUE;
        }
        return FALSE;
    }

    public function delTmp($user_id) {
        $where['user_id'] = $user_id;

        if ($this->db->delete($this->tableName, $where, 1)) {
            return TRUE;
        }
        return FALSE;
    }

    public function addTmp($user_id, $action_key = '', $need_confirm = 0, $json_data = '') {
        $set = array(
            'user_id' => $user_id,
            'action_key' => $action_key,
            'need_confirm' => $need_confirm,
            'json_data' => $json_data,
            'create_time' => time(),
            'update_time' => time()
        );

        $this->db->insert($this->tableName, $set);
        $id = $this->db->insert_id();
        return $this->db->last_query();
        if ($id > 0) {
            return $id;
        }
        return FALSE;
    }

}