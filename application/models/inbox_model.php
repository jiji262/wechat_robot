<?php

/*
 * Inbox Model
 */

class Inbox_model extends CI_Model {
    private $tableName = 'u_m_inbox';
    public function __construct() {
        parent::__construct();
    }

    /*
     * save msg
     * @param   string
     * @param   string
     */

    public function saveMsg($fromUser, $jsonData) {
        $set = array(
            'from_user' => strval($fromUser),
            'json_data' => $jsonData,
            'create_time'=> time()
        );

        $this->db->insert($this->tableName, $set);
//        return $this->db->last_query();
        $id = $this->db->insert_id();
        if ($id > 0) {
            return $id;
        }
        return FALSE;
    }

}