<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index() {
        $this->load->library('wechat');

        //work mode
        $env = '';

        switch ($env) {
            case 'test':
                //for testing
//                $content = 'dc#31';
//                $content = 'gs#3';
//                $content = 'dh#3555555';
//                $content = 'dz#安新南区356栋3楼';
                $content = 'qr#1';
                $xml = '<xml><ToUserName><![CDATA[gh_7a90a1c5cafd]]></ToUserName><FromUserName><![CDATA[ope8Ej_nmoB81OATsJSTCswOy_rQ]]></FromUserName><CreateTime>1364265950</CreateTime><MsgType><![CDATA[text]]></MsgType><Content><![CDATA[' . $content . ']]></Content><MsgId>5859477638296371514</MsgId></xml>';
                $this->wechat->resloveMsg($xml);
                break;
            case 'valid':
                //following code using for valid
                $this->wechat->valid($_GET);
                break;
            default :
                //normal mode
                $postStr = @$GLOBALS['HTTP_RAW_POST_DATA'];
                if ($postStr !== NULL) {
                    $this->wechat->resloveMsg($postStr);
                } else {
                    echo 'no data.';
                }
                break;
        }
    }

}
