<?php if ( ! defined('IN_DILICMS')) exit('No direct script access allowed');
$setting['models']['tmp']=array (
  'id' => '4',
  'name' => 'tmp',
  'description' => '临时数据',
  'perpage' => '20',
  'hasattach' => '0',
  'built_in' => '0',
  'fields' => 
  array (
    25 => 
    array (
      'id' => '25',
      'name' => 'user_id',
      'description' => '用户',
      'model' => '4',
      'type' => 'input',
      'length' => '50',
      'values' => '',
      'width' => '400',
      'height' => '20',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '1',
      'order' => '1',
      'editable' => '1',
    ),
    8 => 
    array (
      'id' => '8',
      'name' => 'action_key',
      'description' => '行为代码',
      'model' => '4',
      'type' => 'input',
      'length' => '50',
      'values' => '',
      'width' => '400',
      'height' => '20',
      'rules' => '',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '1',
      'order' => '2',
      'editable' => '1',
    ),
    9 => 
    array (
      'id' => '9',
      'name' => 'json_data',
      'description' => '临时数据',
      'model' => '4',
      'type' => 'textarea',
      'length' => '999',
      'values' => '',
      'width' => '400',
      'height' => '200',
      'rules' => '',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '1',
      'order' => '3',
      'editable' => '1',
    ),
    26 => 
    array (
      'id' => '26',
      'name' => 'need_confirm',
      'description' => '需要确认',
      'model' => '4',
      'type' => 'select',
      'length' => '1',
      'values' => 
      array (
        0 => '否',
        1 => '是',
      ),
      'width' => '400',
      'height' => '20',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '1',
      'order' => '4',
      'editable' => '1',
    ),
  ),
  'listable' => 
  array (
    0 => '25',
    1 => '8',
    2 => '9',
    3 => '26',
  ),
  'searchable' => 
  array (
  ),
);