<?php if ( ! defined('IN_DILICMS')) exit('No direct script access allowed');
$setting['models']['response']=array (
  'id' => '5',
  'name' => 'response',
  'description' => '预定义回复',
  'perpage' => '20',
  'hasattach' => '1',
  'built_in' => '0',
  'fields' => 
  array (
    10 => 
    array (
      'id' => '10',
      'name' => 'for_keyword_id',
      'description' => '对应关键字',
      'model' => '5',
      'type' => 'content',
      'length' => '10',
      'values' => 'keywords|keyword',
      'width' => '400',
      'height' => '20',
      'rules' => '',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '1',
      'order' => '1',
      'editable' => '1',
    ),
    11 => 
    array (
      'id' => '11',
      'name' => 'msg_type',
      'description' => '回复消息类型',
      'model' => '5',
      'type' => 'select_from_model',
      'length' => '50',
      'values' => 'msgtype|description',
      'width' => '400',
      'height' => '20',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '1',
      'order' => '2',
      'editable' => '1',
    ),
    12 => 
    array (
      'id' => '12',
      'name' => 'content',
      'description' => '文本内容',
      'model' => '5',
      'type' => 'textarea',
      'length' => '999',
      'values' => '',
      'width' => '400',
      'height' => '200',
      'rules' => '',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '0',
      'order' => '3',
      'editable' => '1',
    ),
    13 => 
    array (
      'id' => '13',
      'name' => 'title',
      'description' => '标题',
      'model' => '5',
      'type' => 'input',
      'length' => '50',
      'values' => '',
      'width' => '400',
      'height' => '20',
      'rules' => '',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '0',
      'order' => '4',
      'editable' => '1',
    ),
    14 => 
    array (
      'id' => '14',
      'name' => 'description',
      'description' => '描述',
      'model' => '5',
      'type' => 'textarea',
      'length' => '999',
      'values' => '',
      'width' => '400',
      'height' => '200',
      'rules' => '',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '0',
      'order' => '5',
      'editable' => '1',
    ),
    15 => 
    array (
      'id' => '15',
      'name' => 'pic_url',
      'description' => '图片链接',
      'model' => '5',
      'type' => 'input',
      'length' => '999',
      'values' => '',
      'width' => '400',
      'height' => '20',
      'rules' => '',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '0',
      'order' => '6',
      'editable' => '1',
    ),
    16 => 
    array (
      'id' => '16',
      'name' => 'link_url',
      'description' => '页面链接',
      'model' => '5',
      'type' => 'input',
      'length' => '999',
      'values' => '',
      'width' => '400',
      'height' => '20',
      'rules' => '',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '0',
      'order' => '7',
      'editable' => '1',
    ),
    17 => 
    array (
      'id' => '17',
      'name' => 'music_url',
      'description' => '音乐链接',
      'model' => '5',
      'type' => 'input',
      'length' => '999',
      'values' => '',
      'width' => '400',
      'height' => '20',
      'rules' => '',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '0',
      'order' => '8',
      'editable' => '1',
    ),
    18 => 
    array (
      'id' => '18',
      'name' => 'hqmusic_url',
      'description' => '高品质音乐链接',
      'model' => '5',
      'type' => 'input',
      'length' => '999',
      'values' => '',
      'width' => '400',
      'height' => '20',
      'rules' => '',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '0',
      'order' => '9',
      'editable' => '1',
    ),
  ),
  'listable' => 
  array (
    0 => '10',
    1 => '11',
  ),
  'searchable' => 
  array (
  ),
);