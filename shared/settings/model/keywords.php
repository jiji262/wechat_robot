<?php if ( ! defined('IN_DILICMS')) exit('No direct script access allowed');
$setting['models']['keywords']=array (
  'id' => '3',
  'name' => 'keywords',
  'description' => '关键字',
  'perpage' => '20',
  'hasattach' => '0',
  'built_in' => '0',
  'fields' => 
  array (
    21 => 
    array (
      'id' => '21',
      'name' => 'type',
      'description' => '消息类型',
      'model' => '3',
      'type' => 'select_from_model',
      'length' => '50',
      'values' => 'msgtype|description',
      'width' => '400',
      'height' => '20',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '1',
      'order' => '1',
      'editable' => '1',
    ),
    5 => 
    array (
      'id' => '5',
      'name' => 'keyword',
      'description' => '关键字',
      'model' => '3',
      'type' => 'input',
      'length' => '50',
      'values' => '',
      'width' => '400',
      'height' => '20',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '1',
      'order' => '2',
      'editable' => '1',
    ),
    22 => 
    array (
      'id' => '22',
      'name' => 'description',
      'description' => '描述',
      'model' => '3',
      'type' => 'textarea',
      'length' => '999',
      'values' => '',
      'width' => '400',
      'height' => '200',
      'rules' => '',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '0',
      'order' => '5',
      'editable' => '1',
    ),
  ),
  'listable' => 
  array (
    0 => '21',
    1 => '5',
  ),
  'searchable' => 
  array (
  ),
);