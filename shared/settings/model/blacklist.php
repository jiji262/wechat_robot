<?php if ( ! defined('IN_DILICMS')) exit('No direct script access allowed');
$setting['models']['blacklist']=array (
  'id' => '9',
  'name' => 'blacklist',
  'description' => '黑名单',
  'perpage' => '20',
  'hasattach' => '0',
  'built_in' => '0',
  'fields' => 
  array (
    40 => 
    array (
      'id' => '40',
      'name' => 'opn_id',
      'description' => '用户Id',
      'model' => '9',
      'type' => 'input',
      'length' => '50',
      'values' => '',
      'width' => '400',
      'height' => '20',
      'rules' => 'required',
      'ruledescription' => '',
      'searchable' => '0',
      'listable' => '1',
      'order' => '1',
      'editable' => '1',
    ),
  ),
  'listable' => 
  array (
    0 => '40',
  ),
  'searchable' => 
  array (
  ),
);