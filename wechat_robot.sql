/*
Navicat MySQL Data Transfer

Source Server         : localhost_mysql
Source Server Version : 50524
Source Host           : localhost:3306
Source Database       : bb_robot

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2013-05-02 10:21:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `wxmsg_admins`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_admins`;
CREATE TABLE `wxmsg_admins` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(16) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `role` smallint(5) unsigned DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '1=正常，2=冻结',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`username`),
  KEY `group` (`role`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_admins
-- ----------------------------
INSERT INTO `wxmsg_admins` VALUES ('1', 'admin', '580c64edc2cc6e98da62b1b8314c2a28c6fb22e1', '37374ab159', 'hello@dilicms.com', '1', '1');

-- ----------------------------
-- Table structure for `wxmsg_attachments`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_attachments`;
CREATE TABLE `wxmsg_attachments` (
  `aid` int(10) NOT NULL AUTO_INCREMENT,
  `uid` smallint(10) NOT NULL DEFAULT '0',
  `model` mediumint(10) DEFAULT '0',
  `from` tinyint(1) DEFAULT '0' COMMENT '0:content model,1:cate model',
  `content` int(10) DEFAULT '0',
  `name` varchar(30) DEFAULT NULL,
  `folder` varchar(15) DEFAULT NULL,
  `realname` varchar(50) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `image` tinyint(1) DEFAULT '0',
  `posttime` int(11) DEFAULT '0',
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_attachments
-- ----------------------------
INSERT INTO `wxmsg_attachments` VALUES ('1', '1', '0', '0', '0', '136448031468981a0d689fa8b4', '2013/03', 'xuese', 'jpg', '1', '1364480314');
INSERT INTO `wxmsg_attachments` VALUES ('2', '1', '0', '0', '0', '13644804701f58a153e5e70b74', '2013/03', 'xuese', 'jpg', '1', '1364480470');

-- ----------------------------
-- Table structure for `wxmsg_backend_settings`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_backend_settings`;
CREATE TABLE `wxmsg_backend_settings` (
  `backend_theme` varchar(15) DEFAULT NULL,
  `backend_lang` varchar(10) DEFAULT NULL,
  `backend_root_access` tinyint(1) unsigned DEFAULT '1',
  `backend_access_point` varchar(20) DEFAULT 'admin',
  `backend_title` varchar(100) DEFAULT 'DiliCMS后台管理',
  `backend_logo` varchar(100) DEFAULT 'images/logo.gif'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_backend_settings
-- ----------------------------
INSERT INTO `wxmsg_backend_settings` VALUES ('default', 'zh-cn', '1', '', 'wechat_robot', 'images/logo.gif');

-- ----------------------------
-- Table structure for `wxmsg_cate_fields`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_cate_fields`;
CREATE TABLE `wxmsg_cate_fields` (
  `id` mediumint(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `description` varchar(40) DEFAULT NULL,
  `model` smallint(10) unsigned DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `length` smallint(10) unsigned DEFAULT NULL,
  `values` tinytext,
  `width` smallint(10) DEFAULT NULL,
  `height` smallint(10) DEFAULT NULL,
  `rules` tinytext,
  `ruledescription` tinytext,
  `searchable` tinyint(1) unsigned DEFAULT NULL,
  `listable` tinyint(1) unsigned DEFAULT NULL,
  `order` int(5) unsigned DEFAULT NULL,
  `editable` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`model`),
  KEY `model` (`model`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_cate_fields
-- ----------------------------
INSERT INTO `wxmsg_cate_fields` VALUES ('1', 'type_name', '类型名称', '1', 'input', '50', '', '400', '20', 'required', '', '0', '1', '1', '1');
INSERT INTO `wxmsg_cate_fields` VALUES ('2', 'description', '描述', '1', 'input', '50', '', '400', '20', 'required', '', '0', '1', '2', '1');

-- ----------------------------
-- Table structure for `wxmsg_cate_models`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_cate_models`;
CREATE TABLE `wxmsg_cate_models` (
  `id` mediumint(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(40) NOT NULL,
  `perpage` varchar(2) NOT NULL,
  `level` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `hasattach` tinyint(1) NOT NULL DEFAULT '0',
  `built_in` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_cate_models
-- ----------------------------
INSERT INTO `wxmsg_cate_models` VALUES ('1', 'msgtype', '消息类型', '20', '1', '0', '0');

-- ----------------------------
-- Table structure for `wxmsg_fieldtypes`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_fieldtypes`;
CREATE TABLE `wxmsg_fieldtypes` (
  `k` varchar(20) NOT NULL,
  `v` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_fieldtypes
-- ----------------------------
INSERT INTO `wxmsg_fieldtypes` VALUES ('int', '整形(INT)');
INSERT INTO `wxmsg_fieldtypes` VALUES ('float', '浮点型(FLOAT)');
INSERT INTO `wxmsg_fieldtypes` VALUES ('input', '单行文本框(VARCHAR)');
INSERT INTO `wxmsg_fieldtypes` VALUES ('textarea', '文本区域(VARCHAR)');
INSERT INTO `wxmsg_fieldtypes` VALUES ('select', '下拉菜单(VARCHAR)');
INSERT INTO `wxmsg_fieldtypes` VALUES ('select_from_model', '下拉菜单(模型数据)(INT)');
INSERT INTO `wxmsg_fieldtypes` VALUES ('linked_menu', '联动下拉菜单(VARCHAR)');
INSERT INTO `wxmsg_fieldtypes` VALUES ('radio', '单选按钮(VARCHAR)');
INSERT INTO `wxmsg_fieldtypes` VALUES ('radio_from_model', '单选按钮(模型数据)(INT)');
INSERT INTO `wxmsg_fieldtypes` VALUES ('checkbox', '复选框(VARCHAR)');
INSERT INTO `wxmsg_fieldtypes` VALUES ('checkbox_from_model', '复选框(模型数据)(VARCHAR)');
INSERT INTO `wxmsg_fieldtypes` VALUES ('wysiwyg', '编辑器(TEXT)');
INSERT INTO `wxmsg_fieldtypes` VALUES ('wysiwyg_basic', '编辑器(简)(TEXT)');
INSERT INTO `wxmsg_fieldtypes` VALUES ('datetime', '日期时间(VARCHAR)');
INSERT INTO `wxmsg_fieldtypes` VALUES ('content', '内容模型调用(INT)');

-- ----------------------------
-- Table structure for `wxmsg_menus`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_menus`;
CREATE TABLE `wxmsg_menus` (
  `menu_id` tinyint(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_name` varchar(20) NOT NULL,
  `method_name` varchar(30) NOT NULL,
  `menu_name` varchar(20) NOT NULL,
  `menu_level` tinyint(2) unsigned DEFAULT '0',
  `menu_parent` tinyint(10) unsigned DEFAULT '0',
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_menus
-- ----------------------------
INSERT INTO `wxmsg_menus` VALUES ('1', 'system', 'home', '系统', '0', '0');
INSERT INTO `wxmsg_menus` VALUES ('2', 'system', 'home', '后台首页', '1', '1');
INSERT INTO `wxmsg_menus` VALUES ('3', 'system', 'home', '后台首页', '2', '2');
INSERT INTO `wxmsg_menus` VALUES ('4', 'setting', 'site', '系统设置', '1', '1');
INSERT INTO `wxmsg_menus` VALUES ('5', 'setting', 'site', '站点设置', '2', '4');
INSERT INTO `wxmsg_menus` VALUES ('6', 'setting', 'backend', '后台设置', '2', '4');
INSERT INTO `wxmsg_menus` VALUES ('7', 'system', 'password', '修改密码', '2', '4');
INSERT INTO `wxmsg_menus` VALUES ('8', 'system', 'cache', '更新缓存', '2', '4');
INSERT INTO `wxmsg_menus` VALUES ('9', 'model', 'view', '模型管理', '1', '1');
INSERT INTO `wxmsg_menus` VALUES ('10', 'model', 'view', '内容模型管理', '2', '9');
INSERT INTO `wxmsg_menus` VALUES ('11', 'category', 'view', '分类模型管理', '2', '9');
INSERT INTO `wxmsg_menus` VALUES ('12', 'plugin', 'view', '插件管理', '1', '1');
INSERT INTO `wxmsg_menus` VALUES ('13', 'plugin', 'view', '插件管理', '2', '12');
INSERT INTO `wxmsg_menus` VALUES ('14', 'role', 'view', '权限管理', '1', '1');
INSERT INTO `wxmsg_menus` VALUES ('15', 'role', 'view', '用户组管理', '2', '14');
INSERT INTO `wxmsg_menus` VALUES ('16', 'user', 'view', '用户管理', '2', '14');
INSERT INTO `wxmsg_menus` VALUES ('17', 'content', 'view', '内容管理', '0', '0');
INSERT INTO `wxmsg_menus` VALUES ('18', 'content', 'view', '内容管理', '1', '17');
INSERT INTO `wxmsg_menus` VALUES ('19', 'category_content', 'view', '分类管理', '1', '17');
INSERT INTO `wxmsg_menus` VALUES ('20', 'module', 'run', '工具', '0', '0');

-- ----------------------------
-- Table structure for `wxmsg_models`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_models`;
CREATE TABLE `wxmsg_models` (
  `id` smallint(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(50) NOT NULL,
  `perpage` varchar(2) NOT NULL DEFAULT '10',
  `hasattach` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `built_in` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_models
-- ----------------------------
INSERT INTO `wxmsg_models` VALUES ('1', 'inbox', '收信箱', '20', '0', '0');
INSERT INTO `wxmsg_models` VALUES ('2', 'outbox', '发件箱', '20', '0', '0');
INSERT INTO `wxmsg_models` VALUES ('3', 'keywords', '关键字', '20', '0', '0');
INSERT INTO `wxmsg_models` VALUES ('4', 'tmp', '临时数据', '20', '0', '0');
INSERT INTO `wxmsg_models` VALUES ('5', 'response', '预定义回复', '20', '1', '0');
INSERT INTO `wxmsg_models` VALUES ('8', 'cmd_response', '命令预定义回复', '20', '1', '0');
INSERT INTO `wxmsg_models` VALUES ('9', 'blacklist', '黑名单', '20', '0', '0');

-- ----------------------------
-- Table structure for `wxmsg_model_fields`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_model_fields`;
CREATE TABLE `wxmsg_model_fields` (
  `id` mediumint(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(40) NOT NULL,
  `model` smallint(10) unsigned NOT NULL DEFAULT '0',
  `type` varchar(20) DEFAULT NULL,
  `length` smallint(10) unsigned DEFAULT NULL,
  `values` tinytext NOT NULL,
  `width` smallint(10) unsigned NOT NULL,
  `height` smallint(10) unsigned NOT NULL,
  `rules` tinytext NOT NULL,
  `ruledescription` tinytext NOT NULL,
  `searchable` tinyint(1) unsigned NOT NULL,
  `listable` tinyint(1) unsigned NOT NULL,
  `order` int(5) unsigned DEFAULT NULL,
  `editable` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`model`),
  KEY `model` (`model`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_model_fields
-- ----------------------------
INSERT INTO `wxmsg_model_fields` VALUES ('1', 'from_user', '发信人', '1', 'input', '50', '', '400', '20', 'required', '', '0', '1', '1', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('2', 'json_data', '信息数据', '1', 'textarea', '999', '', '400', '200', 'required', '', '0', '0', '2', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('3', 'to_user', '收信人', '2', 'input', '50', '', '400', '20', 'required', '', '0', '1', '1', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('4', 'xml_data', '信息数据', '2', 'textarea', '999', '', '400', '200', 'required', '', '0', '0', '2', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('5', 'keyword', '关键字', '3', 'input', '50', '', '400', '20', 'required', '', '0', '1', '2', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('8', 'action_key', '行为代码', '4', 'input', '50', '', '400', '20', '', '', '0', '1', '2', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('9', 'json_data', '临时数据', '4', 'textarea', '999', '', '400', '200', '', '', '0', '1', '3', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('10', 'for_keyword_id', '对应关键字', '5', 'content', '10', 'keywords|keyword', '400', '20', '', '', '0', '1', '1', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('11', 'msg_type', '回复消息类型', '5', 'select_from_model', '50', 'msgtype|description', '400', '20', 'required', '', '0', '1', '2', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('12', 'content', '文本内容', '5', 'textarea', '999', '', '400', '200', '', '', '0', '0', '3', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('13', 'title', '标题', '5', 'input', '50', '', '400', '20', '', '', '0', '0', '4', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('14', 'description', '描述', '5', 'textarea', '999', '', '400', '200', '', '', '0', '0', '5', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('15', 'pic_url', '图片链接', '5', 'input', '999', '', '400', '20', '', '', '0', '0', '6', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('16', 'link_url', '页面链接', '5', 'input', '999', '', '400', '20', '', '', '0', '0', '7', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('17', 'music_url', '音乐链接', '5', 'input', '999', '', '400', '20', '', '', '0', '0', '8', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('18', 'hqmusic_url', '高品质音乐链接', '5', 'input', '999', '', '400', '20', '', '', '0', '0', '9', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('22', 'description', '描述', '3', 'textarea', '999', '', '400', '200', '', '', '0', '0', '5', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('21', 'type', '消息类型', '3', 'select_from_model', '50', 'msgtype|description', '400', '20', 'required', '', '0', '1', '1', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('26', 'need_confirm', '需要确认', '4', 'select', '1', '0=否|1=是', '400', '20', 'required', '', '0', '1', '4', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('25', 'user_id', '用户', '4', 'input', '50', '', '400', '20', 'required', '', '0', '1', '1', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('27', 'for_action_key', '目标行为代码', '8', 'input', '50', '', '400', '20', 'required', '', '0', '1', '1', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('28', 'msg_type', '回复消息类型', '8', 'select_from_model', '50', 'msgtype|description', '400', '20', 'required', '', '0', '1', '2', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('29', 'content', '文本内容', '8', 'textarea', '999', '', '400', '200', '', '', '0', '0', '3', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('30', 'title', '标题', '8', 'input', '50', '', '400', '20', '', '', '0', '0', '4', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('31', 'description', '描述', '8', 'input', '50', '', '400', '20', 'required', '', '0', '1', '0', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('32', 'pic_url', '图片链接', '8', 'input', '999', '', '400', '20', '', '', '0', '0', '6', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('33', 'link_url', '页面链接', '8', 'input', '999', '', '400', '20', '', '', '0', '0', '7', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('34', 'music_url', '音乐链接', '8', 'input', '999', '', '400', '20', '', '', '0', '0', '8', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('35', 'hqmusic_url', '高品质音乐链接', '8', 'input', '999', '', '400', '20', '', '', '0', '0', '9', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('36', 'is_expire', '是否有效期', '8', 'select', '10', '0=否|1=是', '400', '20', 'required', '', '0', '1', '10', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('37', 'pkey', '父行为代码', '8', 'content', '10', 'cmd_response|for_action_key', '400', '20', '', '', '0', '1', '12', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('38', 'dreg', '数据格式', '8', 'input', '999', '', '400', '20', '', '', '0', '1', '13', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('39', 'is_need_confirm', '是否需要确认', '8', 'select', '1', '0=否|1=是', '400', '20', 'required', '', '0', '1', '14', '1');
INSERT INTO `wxmsg_model_fields` VALUES ('40', 'opn_id', '用户Id', '9', 'input', '50', '', '400', '20', 'required', '', '0', '1', '1', '1');

-- ----------------------------
-- Table structure for `wxmsg_plugins`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_plugins`;
CREATE TABLE `wxmsg_plugins` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `version` varchar(5) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `author` varchar(20) NOT NULL,
  `link` varchar(100) NOT NULL,
  `copyrights` varchar(100) NOT NULL,
  `access` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_plugins
-- ----------------------------

-- ----------------------------
-- Table structure for `wxmsg_rights`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_rights`;
CREATE TABLE `wxmsg_rights` (
  `right_id` tinyint(10) unsigned NOT NULL AUTO_INCREMENT,
  `right_name` varchar(30) DEFAULT NULL,
  `right_class` varchar(30) DEFAULT NULL,
  `right_method` varchar(30) DEFAULT NULL,
  `right_detail` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`right_id`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_rights
-- ----------------------------
INSERT INTO `wxmsg_rights` VALUES ('1', '密码修改', 'system', 'password', null);
INSERT INTO `wxmsg_rights` VALUES ('2', '更新缓存', 'system', 'cache', null);
INSERT INTO `wxmsg_rights` VALUES ('3', ' 站点设置', 'setting', 'site', null);
INSERT INTO `wxmsg_rights` VALUES ('4', '后台设置', 'setting', 'backend', null);
INSERT INTO `wxmsg_rights` VALUES ('5', '插件管理[列表]', 'plugin', 'view', null);
INSERT INTO `wxmsg_rights` VALUES ('6', '添加插件', 'plugin', 'add', null);
INSERT INTO `wxmsg_rights` VALUES ('7', '修改插件', 'plugin', 'edit', null);
INSERT INTO `wxmsg_rights` VALUES ('8', '卸载插件', 'plugin', 'del', null);
INSERT INTO `wxmsg_rights` VALUES ('9', '导出插件', 'plugin', 'export', null);
INSERT INTO `wxmsg_rights` VALUES ('10', '导入插件', 'plugin', 'import', null);
INSERT INTO `wxmsg_rights` VALUES ('11', '激活插件', 'plugin', 'active', null);
INSERT INTO `wxmsg_rights` VALUES ('12', '禁用插件', 'plugin', 'deactive', null);
INSERT INTO `wxmsg_rights` VALUES ('13', '运行插件', 'module', 'run', null);
INSERT INTO `wxmsg_rights` VALUES ('14', '内容模型管理[列表]', 'model', 'view', null);
INSERT INTO `wxmsg_rights` VALUES ('15', '添加内容模型', 'model', 'add', null);
INSERT INTO `wxmsg_rights` VALUES ('16', '修改内容模型', 'model', 'edit', null);
INSERT INTO `wxmsg_rights` VALUES ('17', '删除内容模型', 'model', 'del', null);
INSERT INTO `wxmsg_rights` VALUES ('18', '内容模型字段管理[列表]', 'model', 'fields', null);
INSERT INTO `wxmsg_rights` VALUES ('19', '添加内容模型字段', 'model', 'add_filed', null);
INSERT INTO `wxmsg_rights` VALUES ('20', '修改内容模型字段', 'model', 'edit_field', null);
INSERT INTO `wxmsg_rights` VALUES ('21', '删除内容模型字段', 'model', 'del_field', null);
INSERT INTO `wxmsg_rights` VALUES ('22', '分类模型管理[列表]', 'category', 'view', null);
INSERT INTO `wxmsg_rights` VALUES ('23', '添加分类模型', 'category', 'add', null);
INSERT INTO `wxmsg_rights` VALUES ('24', '修改分类模型', 'category', 'edit', null);
INSERT INTO `wxmsg_rights` VALUES ('25', '删除分类模型', 'category', 'del', null);
INSERT INTO `wxmsg_rights` VALUES ('26', '分类模型字段管理[列表]', 'category', 'fields', null);
INSERT INTO `wxmsg_rights` VALUES ('27', '添加分类模型字段', 'category', 'add_filed', null);
INSERT INTO `wxmsg_rights` VALUES ('28', '修改分类模型字段', 'category', 'edit_field', null);
INSERT INTO `wxmsg_rights` VALUES ('29', '删除分类模型字段', 'category', 'del_field', null);
INSERT INTO `wxmsg_rights` VALUES ('30', '内容管理[列表]', 'content', 'view', null);
INSERT INTO `wxmsg_rights` VALUES ('31', '添加内容[表单]', 'content', 'form', 'add');
INSERT INTO `wxmsg_rights` VALUES ('32', '修改内容[表单]', 'content', 'form', 'edit');
INSERT INTO `wxmsg_rights` VALUES ('33', '添加内容[动作]', 'content', 'save', 'add');
INSERT INTO `wxmsg_rights` VALUES ('34', '修改内容[动作]', 'content', 'save', 'edit');
INSERT INTO `wxmsg_rights` VALUES ('35', '删除内容', 'content', 'del', null);
INSERT INTO `wxmsg_rights` VALUES ('36', '分类管理[列表]', 'category_content', 'view', null);
INSERT INTO `wxmsg_rights` VALUES ('37', '添加分类[表单]', 'category_content', 'form', 'add');
INSERT INTO `wxmsg_rights` VALUES ('38', '修改分类[表单]', 'category_content', 'form', 'edit');
INSERT INTO `wxmsg_rights` VALUES ('39', '添加分类[动作]', 'category_content', 'save', 'add');
INSERT INTO `wxmsg_rights` VALUES ('40', '修改分类[动作]', 'category_content', 'save', 'edit');
INSERT INTO `wxmsg_rights` VALUES ('41', '删除分类', 'category_content', 'del', null);
INSERT INTO `wxmsg_rights` VALUES ('42', '用户组管理[列表]', 'role', 'view', null);
INSERT INTO `wxmsg_rights` VALUES ('43', '添加用户组', 'role', 'add', null);
INSERT INTO `wxmsg_rights` VALUES ('44', '修改用户组', 'role', 'edit', null);
INSERT INTO `wxmsg_rights` VALUES ('45', '删除用户组', 'role', 'del', null);
INSERT INTO `wxmsg_rights` VALUES ('46', '用户管理[列表]', 'user', 'view', null);
INSERT INTO `wxmsg_rights` VALUES ('47', '添加用户', 'user', 'add', null);
INSERT INTO `wxmsg_rights` VALUES ('48', '修改用户', 'user', 'edit', null);
INSERT INTO `wxmsg_rights` VALUES ('49', '删除用户', 'user', 'del', null);

-- ----------------------------
-- Table structure for `wxmsg_roles`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_roles`;
CREATE TABLE `wxmsg_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `rights` varchar(255) NOT NULL,
  `models` varchar(255) NOT NULL,
  `category_models` varchar(255) NOT NULL,
  `plugins` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_roles
-- ----------------------------
INSERT INTO `wxmsg_roles` VALUES ('1', 'root', '', '', '', '');

-- ----------------------------
-- Table structure for `wxmsg_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_sessions`;
CREATE TABLE `wxmsg_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_sessions
-- ----------------------------
INSERT INTO `wxmsg_sessions` VALUES ('c84aed5b3b9ae17add738713174757e1', '127.0.0.1', 'Mozilla/5.0 (Windows NT 5.2) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.64 Safari/537.31', '1367461160', 'a:2:{s:9:\"user_data\";s:0:\"\";s:3:\"uid\";s:1:\"1\";}');

-- ----------------------------
-- Table structure for `wxmsg_site_settings`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_site_settings`;
CREATE TABLE `wxmsg_site_settings` (
  `site_name` varchar(50) DEFAULT NULL,
  `site_domain` varchar(50) DEFAULT NULL,
  `site_logo` varchar(50) DEFAULT NULL,
  `site_icp` varchar(50) DEFAULT NULL,
  `site_terms` text,
  `site_stats` varchar(200) DEFAULT NULL,
  `site_footer` varchar(500) DEFAULT NULL,
  `site_status` tinyint(1) DEFAULT '1',
  `site_close_reason` varchar(200) DEFAULT NULL,
  `site_keyword` varchar(200) DEFAULT NULL,
  `site_description` varchar(200) DEFAULT NULL,
  `site_theme` varchar(20) DEFAULT NULL,
  `attachment_url` varchar(50) DEFAULT NULL,
  `attachment_dir` varchar(20) DEFAULT NULL,
  `attachment_type` varchar(50) DEFAULT NULL,
  `attachment_maxupload` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_site_settings
-- ----------------------------
INSERT INTO `wxmsg_site_settings` VALUES ('wechat_robot', '', 'images/logo.gif', '', '', '', '', '1', '网站维护升级中......', '', '', 'default', 'http://www.tt.com/weixin_robot/attachments', 'attachments', '*.jpg;*.gif;*.png;*.mp3;*.wav', '2097152');

-- ----------------------------
-- Table structure for `wxmsg_u_c_msgtype`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_u_c_msgtype`;
CREATE TABLE `wxmsg_u_c_msgtype` (
  `classid` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` int(5) unsigned NOT NULL DEFAULT '0',
  `level` int(2) unsigned NOT NULL DEFAULT '1',
  `path` varchar(50) NOT NULL DEFAULT '',
  `type_name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`classid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_u_c_msgtype
-- ----------------------------
INSERT INTO `wxmsg_u_c_msgtype` VALUES ('1', '0', '1', '', 'text', '文本');
INSERT INTO `wxmsg_u_c_msgtype` VALUES ('2', '0', '1', '', 'image', '图片');
INSERT INTO `wxmsg_u_c_msgtype` VALUES ('3', '0', '1', '', 'event', '事件推送');
INSERT INTO `wxmsg_u_c_msgtype` VALUES ('4', '0', '1', '', 'location', '地理位置');
INSERT INTO `wxmsg_u_c_msgtype` VALUES ('5', '0', '1', '', 'link', '链接');
INSERT INTO `wxmsg_u_c_msgtype` VALUES ('6', '0', '1', '', 'news', '图文超链接');
INSERT INTO `wxmsg_u_c_msgtype` VALUES ('7', '0', '1', '', 'voice', '语音');

-- ----------------------------
-- Table structure for `wxmsg_u_m_blacklist`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_u_m_blacklist`;
CREATE TABLE `wxmsg_u_m_blacklist` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `create_time` int(10) unsigned NOT NULL,
  `update_time` int(10) unsigned NOT NULL,
  `create_user` tinyint(10) unsigned NOT NULL,
  `update_user` tinyint(10) unsigned NOT NULL,
  `opn_id` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_u_m_blacklist
-- ----------------------------

-- ----------------------------
-- Table structure for `wxmsg_u_m_cmd_response`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_u_m_cmd_response`;
CREATE TABLE `wxmsg_u_m_cmd_response` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `create_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `update_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `for_action_key` varchar(50) NOT NULL DEFAULT '',
  `msg_type` int(50) NOT NULL DEFAULT '0',
  `content` varchar(999) NOT NULL DEFAULT '',
  `title` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(50) NOT NULL DEFAULT '',
  `pic_url` varchar(999) NOT NULL DEFAULT '',
  `link_url` varchar(999) NOT NULL DEFAULT '',
  `music_url` varchar(999) NOT NULL DEFAULT '',
  `hqmusic_url` varchar(999) NOT NULL DEFAULT '',
  `is_expire` varchar(10) NOT NULL DEFAULT '',
  `pkey` int(10) NOT NULL DEFAULT '0',
  `dreg` varchar(999) NOT NULL DEFAULT '',
  `is_need_confirm` varchar(1) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_u_m_cmd_response
-- ----------------------------

-- ----------------------------
-- Table structure for `wxmsg_u_m_inbox`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_u_m_inbox`;
CREATE TABLE `wxmsg_u_m_inbox` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `create_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `update_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `from_user` varchar(50) NOT NULL DEFAULT '',
  `json_data` varchar(999) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_u_m_inbox
-- ----------------------------

-- ----------------------------
-- Table structure for `wxmsg_u_m_keywords`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_u_m_keywords`;
CREATE TABLE `wxmsg_u_m_keywords` (
  `id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `create_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `update_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `keyword` varchar(50) NOT NULL DEFAULT '',
  `type` int(50) NOT NULL DEFAULT '0',
  `description` varchar(999) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_u_m_keywords
-- ----------------------------

-- ----------------------------
-- Table structure for `wxmsg_u_m_outbox`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_u_m_outbox`;
CREATE TABLE `wxmsg_u_m_outbox` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `create_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `update_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `to_user` varchar(50) NOT NULL DEFAULT '',
  `xml_data` varchar(999) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_u_m_outbox
-- ----------------------------

-- ----------------------------
-- Table structure for `wxmsg_u_m_response`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_u_m_response`;
CREATE TABLE `wxmsg_u_m_response` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `create_time` int(10) unsigned NOT NULL,
  `update_time` int(10) unsigned NOT NULL,
  `create_user` tinyint(10) unsigned NOT NULL,
  `update_user` tinyint(10) unsigned NOT NULL,
  `for_keyword_id` int(10) NOT NULL DEFAULT '0',
  `msg_type` int(50) NOT NULL DEFAULT '0',
  `content` varchar(999) NOT NULL DEFAULT '',
  `title` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(999) NOT NULL DEFAULT '',
  `pic_url` varchar(999) NOT NULL DEFAULT '',
  `link_url` varchar(999) NOT NULL DEFAULT '',
  `music_url` varchar(999) NOT NULL DEFAULT '',
  `hqmusic_url` varchar(999) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_u_m_response
-- ----------------------------

-- ----------------------------
-- Table structure for `wxmsg_u_m_tmp`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_u_m_tmp`;
CREATE TABLE `wxmsg_u_m_tmp` (
  `id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `create_time` int(10) unsigned NOT NULL DEFAULT '0',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0',
  `create_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `update_user` tinyint(10) unsigned NOT NULL DEFAULT '0',
  `action_key` varchar(50) NOT NULL DEFAULT '',
  `json_data` varchar(999) NOT NULL DEFAULT '',
  `user_id` varchar(50) NOT NULL DEFAULT '',
  `need_confirm` varchar(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_u_m_tmp
-- ----------------------------

-- ----------------------------
-- Table structure for `wxmsg_validations`
-- ----------------------------
DROP TABLE IF EXISTS `wxmsg_validations`;
CREATE TABLE `wxmsg_validations` (
  `k` varchar(20) DEFAULT NULL,
  `v` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wxmsg_validations
-- ----------------------------
INSERT INTO `wxmsg_validations` VALUES ('required', '必填');
INSERT INTO `wxmsg_validations` VALUES ('valid_email', 'E-mail格式');
